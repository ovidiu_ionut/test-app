import React, {Fragment} from 'react';
import Logo from '../Logo/Logo';
import NavigationItems from '../Navigation/NavigationItems/NavigationItems';
import './SideDrawer.scss';
import Backdrop from '../UI/Backdrop/Backdrop';

const sideDrawer = (props) => {
    let attachedClasses = ["side-drawer", "close"];
    if (props.open) {
        attachedClasses = ["side-drawer", "open"];
    }

    return (
        <Fragment>
        <Backdrop show={props.open} clicked={props.closed}/>
        <div className={attachedClasses.join(' ')}>
            <div className="logo-wrapper-side">
            <Logo/>
            </div>
            <nav className="sidebar-wrapper">
                <NavigationItems/>
            </nav>
        </div>
        </Fragment>
    );
};

export default sideDrawer;