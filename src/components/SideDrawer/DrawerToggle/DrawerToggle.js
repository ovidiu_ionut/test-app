import React from 'react';
import './DrawerToggle.scss';

const drawerToggle = (props) => (
    <div id="drawer-toggle" className="material-icons" onClick={props.clicked}>
        format_align_left

        <div></div>
        <div></div>
        <div></div>
    </div>
);

export default drawerToggle;