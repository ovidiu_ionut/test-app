import React from 'react';
import './Toolbar.scss';
import Logo from '../Logo/Logo';
import NavigationItems from '../Navigation/NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';

const toolbar = (props) => (
    <header className="toolbar">
        <DrawerToggle clicked={props.drawerToggleClicked}/>
        <span className="widgets">WIDGETS</span>
        <div className="logo-wrapper">
        <Logo/>
        </div>
        <span id="add-icon" className="material-icons">add</span>
        <nav className="desktop-only">
            <NavigationItems/>
        </nav>
    </header>
);

export default toolbar;