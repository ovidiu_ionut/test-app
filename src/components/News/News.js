import React from 'react';
import './News.scss';

const news = (props) => (
    <div className="news">
        <img src={props.img} alt="news" />
        <h1>{props.title}</h1>
        <div className="footer-text-box">
            <div className="left-side">
                <div className="item"><span id="icon" className="material-icons">
                    link</span>{props.site}</div>
                <div className="item"><span id="icon" className="material-icons">
                    access_time</span>{props.time}</div>
            </div>
            <div className="right-side">
                <div>{props.category}</div>
                <div className="bottom-line"></div>

            </div>
        </div>
    </div>
);

export default news;