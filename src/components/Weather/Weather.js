import React, { Component } from 'react';
import api from '../../config/api';
import './Weather.scss';

class Weather extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
        };
    }

    componentDidMount() {
        fetch(`${api.base}weather?q=Bucharest&appid=${api.key}&units=metric`)
            .then(response => response.json())
            .then(data => this.setState({ data }));
    }

    render() {

        const data = this.state.data;

        return (
            <div className="weather">
                <div className="forecast">
                    <span>TODAY</span>
                    <span>TOMORROW</span>
                    <span>WEEK</span>
                </div>
                {data ? 
                <div className="base-information">
                <div className="left-info">
                    <span id="weather-icon" className="material-icons">cloud_queue</span>
                    <div className="description">{data.weather[0].description}</div>
                    <div className="city">{data.name}</div>
                </div>
                <div className="right-info">{`${data.main.temp.toFixed(0)}°`}</div>
            </div> : ""
                }
            </div>
        )
    }
}

export default Weather;