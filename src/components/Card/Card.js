import React from 'react';
import './Card.scss';

const Card = (props) => (
    <div className="card">
        <div className="card-left">
            <div className="title">{props.title}</div>
            <div className="number">{props.number}</div>
        </div>
        <div className={props.percentage > 0 ? "percentage green": "percentage red"}>{props.percentage > 0 ? `+${props.percentage}`: props.percentage}</div>
    </div>
)

export default Card;