import React, { Component } from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';

class CustomAreaChart extends Component {
    render() {
        const data = [
            { name: 'Apr', num: 90 },
            { name: 'May', num: 70 },
            { name: 'June', num: 100 },
            { name: 'Jul', num: 80 },
            { name: 'Aug', num: 110 },
            { name: 'Sep', num: 95 },
            { name: 'Oct', num: 120 },
            { name: 'Nov', num: 100 },
            { name: 'Dec', num: 95 }
        ];

        return (
            <div className="area-chart-container">
                <AreaChart width={800} height={300} data={data}
                    margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
                    <XAxis dataKey="name" />
                    <YAxis orientation="right"/>
                    <Tooltip />
                    <Area type='monotone' dataKey='num' stroke='rgba(0,0,0,0.2)' strokeWidth={10} fill='#ffffff' />
                </AreaChart>
            </div>
        )
    }
}

export default CustomAreaChart;