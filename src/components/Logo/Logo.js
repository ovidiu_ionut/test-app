import React from 'react';
import nLogo from '../../assets/images/n-logo-orange.png';
import './Logo.scss';

const logo = (props) => (
    <div className="logo" style={{height: props.height}}>
        <img src={nLogo} alt="Logo"/>
    </div>
);

export default logo;