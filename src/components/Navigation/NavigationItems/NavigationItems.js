import React from 'react';
import './NavigationItems.scss';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = () => (
    <ul className="navigation-items">
        <NavigationItem link="/"><span className="material-icons">
            search
</span></NavigationItem>
        <NavigationItem link="/orders">Profile</NavigationItem>
        <NavigationItem link="/auth"><span className="material-icons">
            bookmark_border
</span></NavigationItem>
    </ul>
);

export default navigationItems;