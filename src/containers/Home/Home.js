import React, { Component } from 'react';
import CustomAreaChart from '../../components/Chart/Chart';
import newsImg from '../../assets/images/news.jpg';
import News from '../../components/News/News';
import Weather from '../../components/Weather/Weather';
import Card from '../../components/Card/Card';
import './Home.scss';

class Home extends Component {
    render() {
        const newsData = {
            img: newsImg,
            title: 'Astronauts could land on Red Planet by 2039',
            site: 'SPACE.com',
            time: '20m ago',
            category: 'SCIENCE'
        }

        const stocks = [
            {
                title: 'NASDAQ',
                number: 5055.55,
                percentage: -0.85
            },
            {
                title: 'APPL',
                number: 126.56,
                percentage: 0.51
            },
            {
                title: 'DOW J',
                number: 19926.02,
                percentage: -0.34
            },
            {
                title: 'GOOG',
                number: 534.53,
                percentage: 0.70
            }
        ];

        return (
            <div className="container">
                <div className="first-half">
                    <News img={newsData.img}
                        title={newsData.title}
                        site={newsData.site}
                        time={newsData.time}
                        category={newsData.category} />
                    <Weather />
                </div>
                <div className="second-half">
                    <div className="data-stack">
                        {stocks.map(item => 
                            <Card 
                            key={item.title}
                            title={item.title} 
                            number={item.number} 
                            percentage={item.percentage}/>)}
                    </div>
                    <CustomAreaChart />
                </div>
            </div>
        )
    }
}

export default Home;